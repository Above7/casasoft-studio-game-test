using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour
{
    public GameObject ObjectSpawn;

    void Start()
    {
        GameObject G = Instantiate(ObjectSpawn, transform.position, transform.rotation);
    }
}
