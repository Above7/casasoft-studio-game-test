using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HP_Bar : MonoBehaviour
{
    #region Variable
    public GameObject HpBarDelay;
    public Text text;
    private float SaveHP;
    private Status Status;
    public string StatusTag;
    #endregion

    private void Update()
    {
        if (Status == null)
        {
            Status = GameObject.FindGameObjectWithTag(StatusTag).GetComponent<Status>();
        }

        if (SaveHP != Status.CurrentHP)
        {
            SaveHP = Status.CurrentHP;
            HP_Text();
            playerUI_Method();
            StartCoroutine(HpDelay(HpBarDelay));
        }
    }

    #region Method
    private void HP_Text()
    {
        text.text = Status.CurrentHP.ToString();
    }

    private void playerUI_Method()
    {
        if (Status.CurrentHP > 0)
        {
            transform.localScale = new Vector3(Status.CurrentHP / Status.MaxHP, 1, 1);
        }
        else
        {
            Status.CurrentHP = 0;
            transform.localScale = new Vector3(0, 1, 1);
        }
    }

    private IEnumerator HpDelay(GameObject G)
    {
        while (G.transform.localScale.x > transform.localScale.x)
        {
            G.transform.localScale = new Vector3(G.transform.localScale.x - (0.08f * Time.deltaTime), G.transform.localScale.y, G.transform.localScale.z);
            yield return null;
        }

        G.transform.localScale = transform.localScale;
    }
    #endregion
}
