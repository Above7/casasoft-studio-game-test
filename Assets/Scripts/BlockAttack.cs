﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Hitblock สำหรับการโจมตี
/// </summary>
public class BlockAttack : MonoBehaviour
{
    public float DMG;
    /// <summary>
    /// เป้าหมายของการถูกโจมตี
    /// </summary>
    public string TargetTag;
    /// <summary>
    /// สคิปสเตตัสของเจ้าของการโจมตี
    /// </summary>
    public Status MyStatus;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Status>() && other.tag == TargetTag)
        {
            if (TargetTag == "Player" && other.gameObject.GetComponent<PlayerController>().ImParrying == true)
            {
                MyStatus.SendMessage("GetStun_M");
            }
            else
            {
                other.gameObject.SendMessage("TakeDmg", MyStatus.AttackDMG);
            }
        }
    }    
}
