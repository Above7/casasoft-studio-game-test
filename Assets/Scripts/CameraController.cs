using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    #region Variable
    [Header("Component")]
    public Transform Target;
    public Transform CameraTransform;
    private Camera Camera;

    [Header("Normal Variable")]
    public float Distance;
    public float CurrentX;
    public float CurrentY;
    public float SensivityX;
    public float SensivityY;
    public float Y_Angle_Min;
    public float Y_Angle_Mix;
    #endregion

    #region Unity Method
    private void Start()
    {
        if (Target == null) { Target = GameObject.Find("PositionForCamera").transform; }
        CameraTransform = transform;
        Camera = Camera.main;
    }

    private void Update()
    {
        CurrentX += Input.GetAxis("Mouse X") * SensivityX;
        CurrentY -= Input.GetAxis("Mouse Y") * SensivityY;

        CurrentY = Mathf.Clamp(CurrentY, Y_Angle_Min, Y_Angle_Mix);
    }

    private void LateUpdate()
    {
        Vector3 Direction = new Vector3(0, 0, -Distance);
        Quaternion Rotation = Quaternion.Euler(CurrentY, CurrentX, 0);
        CameraTransform.position = Target.position + Rotation * Direction;
        CameraTransform.LookAt(Target.position);
    }
    #endregion
}
