﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    #region Variable
    [Header("Component")]
    public Rigidbody rigi;
    public Status Status;
    public Animator anim;
    public Collider Player;

    [Header("Normal Variable")]
    public float Radius = 10;
    private Collider[] IKnowThisCollider;
    public float distance_now;
    private Coroutine Co_MoveToPlayer;
    public bool IsResting = false;
    public bool IsStunning = false;
    #endregion

    #region Unity Method
    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(0, 0, 0, 0.2f);
        Gizmos.DrawWireSphere(transform.position, Radius);
    }

    private void Update()
    {
        Dead();
        if (Status.IsDeath == false)
        {
            Resting();
            Behavior();
        }
    }
    #endregion

    #region Method
    public void Resting()
    {
        if (IsResting == true)
        {
            anim.SetBool("Resting", true);
        }
        else
        {
            anim.SetBool("Resting", false);
        }
    }
    public void GetStun_M()
    {
        StartCoroutine(GetStun());
    }
    private IEnumerator GetStun()
    {
        IsStunning = true;
        anim.SetTrigger("Stun");

        yield return new WaitForSeconds(2);

        IsStunning = false;
    }

    private void Behavior()
    {
        //หาผู้เล่น
        IKnowThisCollider = Physics.OverlapSphere(transform.position, Radius, LayerMask.GetMask("Player"));

        if (IKnowThisCollider.Length != 0)
        {
            Player = IKnowThisCollider[0];

            if (IsResting == false && IsStunning == false)
            {
                //walk to player
                if (GameManager.gameManager.PlayerStatus.IsDeath == false)
                {
                    if (anim.GetCurrentAnimatorStateInfo(0).IsName("Enemy_Idle") == true)
                    {
                        if (Co_MoveToPlayer != null)
                        {
                            StopCoroutine(Co_MoveToPlayer);
                        }
                        Co_MoveToPlayer = StartCoroutine(MoveToPlayer());
                        if (Player != null)
                        {
                            MoveToPlayer();
                        }
                    }
                    else if (anim.GetCurrentAnimatorStateInfo(0).IsTag("Attack") == true)
                    {
                        if (Co_MoveToPlayer != null)
                        {
                            StopCoroutine(Co_MoveToPlayer);
                            anim.SetBool("Run", false);
                        }
                    }

                    //Attack
                    if (distance_now <= 1.6f)
                    {
                        if (anim.GetCurrentAnimatorStateInfo(0).IsTag("Attack") == false && IsResting == false)
                        {
                            Attack();
                        }
                    }
                }
            }
        }
        else
        {
            if (Co_MoveToPlayer != null)
            {
                StopCoroutine(Co_MoveToPlayer);
                anim.SetBool("Run", false);
            }
            anim.SetBool("Attack1", false);
            anim.SetBool("Attack2", false);
            Player = null;
        }
    }

    private IEnumerator MoveToPlayer()
    {
        while (Vector3.Distance(transform.position, Player.transform.position) > 1.5f)
        {
            anim.SetBool("Run", true);
            transform.LookAt(Player.transform.position);
            distance_now = Vector3.Distance(transform.position, Player.transform.position);//ระยะห่างในปัจจุบัน
            transform.position = Vector3.Lerp(transform.position, Player.transform.position, Status.MoveSpeed / distance_now * Time.deltaTime);

            yield return null;
        }
        anim.SetBool("Run", false);
        yield break;
    }

    private void Attack()
    {
        int a = Random.Range(0, 2);
        if (a == 0)
        {
            transform.LookAt(Player.transform.position);
            anim.SetBool("Attack2", false);
            anim.SetBool("Attack1", true);
        }
        else if (a == 1)
        {
            transform.LookAt(Player.transform.position);
            anim.SetBool("Attack1", false);
            anim.SetBool("Attack2", true);
        }
    }

    private void Dead()
    {
        if (Status.IsDeath == true && Status.Dying == false)
        {
            Status.Dying = true;
            anim.SetTrigger("Dead");
        }
    }
    #endregion
}
