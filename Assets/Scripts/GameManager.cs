using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager gameManager;

    #region Variable
    [Header("Component")]
    public CameraController CameraController;
    public PlayerController PlayerController;
    public Status PlayerStatus;
    public GameObject Player;
    public GameObject UI_Restart;
    #endregion

    private void Start()
    {
        if (CameraController == null) { CameraController = FindObjectOfType<CameraController>(); }
        if (PlayerController == null) { PlayerController = FindObjectOfType<PlayerController>(); }
        if (Player == null) { Player = GameObject.FindGameObjectWithTag("Player"); }
        if (PlayerStatus == null) { PlayerStatus = GameObject.FindGameObjectWithTag("Player").GetComponent<Status>(); }
    }

    private void Awake()
    {
        if (gameManager != null)
            Destroy(gameManager.gameObject);
        gameManager = this;
    }
}
