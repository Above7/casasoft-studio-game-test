﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Status : MonoBehaviour
{
    #region Variable
    [Header("Status")]
    public float MaxHP = 100;
    public float CurrentHP;
    public float AttackDMG = 10;
    public float MoveSpeed = 10;
    public float AttackSpeed;

    public bool IsDeath = false;
    public bool Dying = false;//false เท่ากับยังไม่เคยเข้าอนิเมชั่นตาย, true เคยแล้ว
    #endregion

    public void TakeDmg(float DMG)
    {
        CurrentHP -= DMG;
        //Debug.Log(gameObject.name + " :  DMG Receive = " + DMG);
    }

    #region Unity Method
    private void Start()
    {
        CurrentHP = MaxHP;
    }

    private void Update()
    {
        if (CurrentHP <= 0)
        {
            IsDeath = true;

            GameManager.gameManager.UI_Restart.SetActive(true);
            GameManager.gameManager.PlayerController.CursorVisible_On();
        }
    }
    #endregion
}
