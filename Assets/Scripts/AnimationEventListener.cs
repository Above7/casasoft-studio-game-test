using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventListener : MonoBehaviour
{
    public GameObject BlockAttackSword;
    public AIController AIController;

    public void OpenCollider_BlockAttack()
    {
        StartCoroutine(BlockAttack());
    }
    private IEnumerator BlockAttack()
    {
        BlockAttackSword.SetActive(true);

        yield return new WaitForSeconds(0.15f);

        BlockAttackSword.SetActive(false);
    }


    public void GetRest_M()
    {
        StartCoroutine(GetRest());
    }
    private IEnumerator GetRest()
    {
        AIController.IsResting = true;

        yield return new WaitForSeconds(1f);

        AIController.IsResting = false;
    }
}
