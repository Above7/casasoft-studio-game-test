﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region Variable
    [Header("Component")]
    public CharacterController Controller;
    public Animator anim;

    [Header("Normal Variable")]
    public float TurnSmoothTime = 0.025f;
    private bool Cursor_IsHiding = true;
    private float TurnSmoothVelocity;
    public bool ImParrying = false;
    public float ParryRemainingTime = 0.5f;
    public float time_count = 0;
    #endregion

    #region Unity Method
    private void Start()
    {
        //ปิดการมองเห็นเมาส์
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        Dead();
        CursorVisible();

        if (GameManager.gameManager.PlayerStatus.IsDeath == false)
        {
            Attack();
            ParryTimeCount();

            if (anim.GetCurrentAnimatorStateInfo(0).IsName("Idle") || anim.GetCurrentAnimatorStateInfo(0).IsName("Run"))
            {
                Parry();
            }

            if (!anim.GetCurrentAnimatorStateInfo(0).IsTag("Attack") && !anim.GetCurrentAnimatorStateInfo(0).IsTag("Parry"))
            {
                Move();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "BlockAttack" && ImParrying == true)
        {
            anim.SetTrigger("ParrySuccass");
            anim.SetBool("Parry", false);
            ImParrying = false;
        }
    }
    #endregion

    #region Method
    private void Move()
    {
        float Horizontal = Input.GetAxisRaw("Horizontal");
        float Vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(Horizontal, Vertical, 0).normalized;

        if (direction.magnitude >= 0.1f)
        {
            float TargetAngle = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg + GameManager.gameManager.CameraController.CameraTransform.eulerAngles.y;
            float Angel = Mathf.SmoothDampAngle(transform.eulerAngles.y, TargetAngle, ref TurnSmoothVelocity, TurnSmoothTime);
            transform.rotation = Quaternion.Euler(0, Angel, 0);

            Vector3 MoveDirection = Quaternion.Euler(0, TargetAngle, 0) * Vector3.forward;
            Controller.Move(MoveDirection.normalized * GameManager.gameManager.PlayerStatus.MoveSpeed * Time.deltaTime);
        }

        if (Vertical == 0 && Horizontal == 0)
        {
            anim.SetBool("Run", false);
        }
        else
        {
            anim.SetBool("Run", true);
        }
    }

    private void CursorVisible()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl) && Cursor_IsHiding == true) //ถ้าปิดแสดงการมองเห็นเมาส์อยู่ให้เปิด
        {
            CursorVisible_On();
        }
        else if (Input.GetKeyDown(KeyCode.LeftControl) && Cursor_IsHiding == false) //ถ้าเปิดแสดงการมองเห็นเมาส์อยู่ให้ปิด
        {
            CursorVisible_Off();
        }
    }

    public void CursorVisible_On()
    {
        Cursor.visible = true;
        Cursor_IsHiding = false;
        Cursor.lockState = CursorLockMode.None;
        GameManager.gameManager.CameraController.enabled = false;
    }

    private void CursorVisible_Off()
    {
        Cursor.visible = false;
        Cursor_IsHiding = true;
        Cursor.lockState = CursorLockMode.Locked;
        GameManager.gameManager.CameraController.enabled = true;
    }

    public void Attack()
    {
        if (Input.GetMouseButtonDown(0))
        {
            anim.SetTrigger("Attack");
        }
    }

    public void Parry()
    {
        if (Input.GetMouseButton(1))
        {
            ImParrying = true;
            anim.SetBool("Parry", true);
        }
        else if (Input.GetMouseButtonUp(1))
        {
            ImParrying = false;
            anim.SetBool("Parry", false);
        }
    }
    public void ParryTimeCount()
    {
        if (time_count < ParryRemainingTime)
        {
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("Parry"))
            {
                time_count += Time.deltaTime;
            }
            else
            {
                time_count = 0;
            }
        }
        else
        {
            time_count = 0;
            ImParrying = false;
            anim.SetBool("Parry", false);
        }
    }

    private void Dead()
    {
        if (GameManager.gameManager.PlayerStatus.IsDeath == true && GameManager.gameManager.PlayerStatus.Dying == false)
        {
            GameManager.gameManager.PlayerStatus.Dying = true;
            anim.SetTrigger("Dead");
        }
    }
    #endregion
}
